# 1_chat

A concurrent chat system in Go and HTML/JavaScript, built for an assignment at Universidade Federal do Rio Grande (http://furg.br).

## Programmers

Below is a list of the programmers that worked on both the client and server of the
project:

 * Gabriel Rodrigues (108311)
 * Giovanni Giacomo (108307)
 * Lucas Viana (108347)
 * Rael Cappra (108344)

## Cloning the Repository

Execute the following command in your shell to clone the repository:

```sh
$ git clone --recursive https://gitlab.com/giovgiac/1_chat.git
```

If you cloned the repository without adding the recursive option, you can fix
that by running the following commands in your shell:

```sh
$ git submodule sync --recursive
$ git submodule update --init --recursive
```
