// Copyright (c) 2019 Team 1. All Rights Reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package main

import (
	"chat"
	"flag"
	"github.com/streadway/amqp"
	"log"
	"net/http"
	"path"
	"runtime"
)

// Flags that are used to configure the server.
var (
	// address is used by the ListenAndServe function to specify the
	// IP address and port that are to be used with the web service.
	address = flag.String("addr", ":8080", "the ip address and port to use with the service")

	// room is used to create the room and join the specific
	// overlay network for this instance of the server.
	room = flag.String("room", "General", "the room that this server belongs to")

	// url is used by the ServeFile function to pick the main web page
	// that is going to be presented at '/'.
	uri = flag.String("uri", "client/", "the web page to serve to the user")
)

// A handle that serves the main ('/') web page to a requesting user.
//
// This function transmits the index page to the user's and blocks
// any attempts to utilize methods other than GET.
func serveWebPage(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
		return
	}

	if r.URL.Path == "" || r.URL.Path == "/" {
		http.ServeFile(w, r, "client/index.html")
	} else {
		http.ServeFile(w, r, path.Join(*uri, r.URL.Path))
	}
}

func newServer() (*chat.Server, error) {
	conn, err := amqp.Dial("amqp://admin:admin@rabbit-master:5672/")
	if err != nil {
		return nil, err
	}

	ch, err := conn.Channel()
	if err != nil {
		return nil, err
	}

	return chat.NewServer(conn, ch /* q, */, *room), nil
}

func main() {
	flag.Parse()

	// Enable All Core Usage
	runtime.GOMAXPROCS(runtime.NumCPU())

	// Create and Process Server on Separate Thread
	server, err := newServer()
	if err != nil {
		log.Fatal(err)
		return
	}

	go server.Process()

	http.HandleFunc("/", serveWebPage)
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		chat.ServeWebServer(server, w, r, *room)
	})

	err = http.ListenAndServe(*address, nil)
	if err != nil {
		log.Fatal("Error while Listening/Serving: ", err)
	}
}
