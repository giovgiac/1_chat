// Copyright (c) 2019 Team 1. All Rights Reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package chat

import (
	"github.com/streadway/amqp"
	"log"
)

// A room is a hub that allows multiple connected clients to
// exchange messages.
//
// The room structure keeps track of join, leave and send requests
// so that it can relay messages to all connected clients, as well as
// allow them to leave or join different rooms at any given time.
//
type Room struct {
	// clients is a dictionary that maps Client pointers to booleans.
	// This boolean indicates whether the client is in the room.
	clients map[*Client]bool

	// join is a go room that receives Client pointers requesting
	// to enter the room.
	join chan *Client

	// leave is a go room that receives Client pointers requesting
	// to leave the room.
	leave chan *Client

	// A message channel to send messages to the
	// exchange of RabbitMQ.
	messages chan []byte

	// messages is a go channel that receives raw bytes that are to be
	// relayed to all connected clients.
	payloads chan []byte

	// name is a string that contains the name of the room.
	name string

	// The queue of RabbitMQ.
	queue amqp.Queue
}

// A function that creates and returns a new room given its name.
//
// This function creates a new room and initializes go channels
// for its individual components, as well as a map containing the
// connected clients.
func newRoom(name string, ch *amqp.Channel) *Room {
	room := Room{
		clients:  make(map[*Client]bool),
		join:     make(chan *Client),
		leave:    make(chan *Client),
		messages: make(chan []byte),
		payloads: make(chan []byte),
		name:     name,
	}

	// Declares the kind of exchange required for RabbitMQ queue.
	err := ch.ExchangeDeclare(
		name,
		"topic",
		true,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		return nil
	}

	// Declares a unique queue for the server channel in RabbitMQ.
	room.queue, err = ch.QueueDeclare(
		"",
		false,
		false,
		true,
		false,
		nil,
	)

	if err != nil {
		return nil
	}

	return &room
}

// The function that loops over the room responsabilities.
//
// This function goes over all of its go channels and verify if
// they contain any pending operations to be performed, such as
// adding a new client, removing an existing one or relaying a
// given message.
//
// WARNING: THIS FUNCTION SHOULD BE CALLED ON A SEPARATE THREAD.
func (r *Room) process() {
	for {
		select {
		case client := <-r.join:
			r.clients[client] = true
			client.room = r

			log.Printf("Client %s joined the room %s\n", client.id, r.name)
			log.Printf("Total Connections on room %s: %d\n", r.name, len(r.clients))

			// If unbound, bind to RabbitMQ.
			if len(r.clients) == 1 {
				err := client.server.channel.QueueBind(
					r.queue.Name,
					"",
					r.name,
					false,
					nil,
				)

				if err != nil {
					log.Printf("Failed to Bind Room: %s\n", r.name)
					continue
				} else {
					log.Printf("Bound Room: %s\n", r.name)
				}
			}
		case client := <-r.leave:
			if _, found := r.clients[client]; found {
				delete(r.clients, client)

				log.Printf("Client %s left the room %s\n", client.id, r.name)
				log.Printf("Total Connections on room %s: %d\n", r.name, len(r.clients))

				// If no more clients, unbind from RabbitMQ.
				if len(r.clients) == 0 {
					err := client.server.channel.QueueUnbind(
						r.queue.Name,
						"",
						r.name,
						nil,
					)

					if err != nil {
						log.Printf("Failed to Unbind Room: %s\n", r.name)
						continue
					} else {
						log.Printf("Unbound Room: %s\n", r.name)
					}
				}
			}
		case msg := <-r.payloads:
			for client := range r.clients {
				select {
				case client.messages <- msg:
				default:
					delete(r.clients, client)
					client.room = nil
				}
			}
		}
	}
}

func (r *Room) read(s *Server) {
	msgs, err := s.channel.Consume(
		r.queue.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		log.Fatal(err)
		return
	}

	for {
		for msg := range msgs {
			log.Printf("Received Message from Exchange %s: %s\n", msg.Exchange, msg.Body)
			handleRoomState(msg.Body, r)
		}
	}
}

func (r *Room) write(s *Server) {
	for {
		select {
		case msg := <-r.messages:
			err := s.channel.Publish(
				r.name,
				"",
				false,
				false,
				amqp.Publishing{
					ContentType: "text/plain",
					Body:        msg,
				},
			)

			if err != nil {
				log.Printf("Error while Sending Message to Exchange %s: %s\n", r.name, msg)
				return
			}
		}
	}
}
