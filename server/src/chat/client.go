// Copyright (c) 2019 Team 1. All Rights Reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package chat

import (
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"log"
)

// The structure that contains the information about
// the client.
type Client struct {
	// The room that the client is connected to.
	room *Room

	// The WebSocket object that controls the client's
	// connection to the server.
	connection *websocket.Conn

	// Stores the string version of the client's UUID.
	id string

	// Stores the nickname of the client, and is used
	// when relaying messages.
	nickname string

	// A go channel for storing messages that are to be
	// sent to this client.
	messages chan []byte

	// Stores the server to which the client is
	// connected to.
	server *Server
}

// A function that creates a new client, with the standard
// nickname 'Unnamed' and a randomly generated UUID, given
// a connection and the server to which the client is
// connected to.
func newClient(server *Server, conn *websocket.Conn) *Client {
	return &Client{
		room:       nil,
		connection: conn,
		id:         uuid.Must(uuid.NewRandom()).String(),
		nickname:   "Unnamed",
		messages:   make(chan []byte, 256),
		server:     server,
	}
}

// A function that reads messages from the client's go channels
// and sends them to be processed by the handlePayload function.
func (c *Client) read() {
	defer func() {
		c.server.unregister <- c
		c.room.leave <- c
		c.connection.Close()
	}()

	for {
		_, payload, err := c.connection.ReadMessage()

		if err != nil {
			log.Printf("Error while Reading InMessage from Client %s: %s", c.id, err)
			return
		}

		handlePayload(payload, c)
	}
}

// A function that writes messages from the client's go channels
// to the WebSocket connection, therefore sending those
// messages to the client.
func (c *Client) write() {
	defer func() {
		c.connection.Close()
	}()

	for {
		select {
		case msg := <-c.messages:
			err := c.connection.WriteMessage(1, msg)

			if err != nil {
				log.Printf("Error while Sending InMessage: %s to %s\n", msg, c.id)
				return
			}
		}
	}
}
