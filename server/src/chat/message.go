// Copyright (c) 2019 Team 1. All Rights Reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package chat

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
)

const (
	// This defines the string that communicates the
	// join room action between client and server.
	ACTION_JOIN = "join"

	// This defines the string that communicates the
	// leave room action between client and server.
	ACTION_LEAVE = "leave"

	// This defines the string that communicates the
	// change name action between client and server.
	ACTION_NAME = "name"

	// This defines the string that communicates to
	// the server that the client wants a list of
	// the available rooms.
	ACTION_ROOMS = "channels"

	// This defines the string that communicates the
	// send message action between client and server.
	ACTION_SEND = "send"

	// This defines the string that communicates to
	// the server that the client wants a list of
	// clients in the room.
	ACTION_USERS = "users"

	// This defines the string that communicates to
	// the server that the client intends to create
	// a new room.
	ACTION_CREATE = "create"

	// This defines the maximum size of a message to
	// be relayed by the server.
	MESSAGE_MAX_SIZE = 4096

	// This defines the maximum size of a name to be
	// set in the server.
	NAME_MAX_SIZE = 32
)

const (
	// This defines the name of the SenderType when
	// it's a user message.
	TYPE_USER = "user"

	// This defines the name of the SenderType when
	// it's a room message.
	TYPE_ROOM = "room"

	// This defines the name of the SenderType when
	// it's a server message.
	TYPE_SERVER = "server"
)

// The InMessage is the structure that controls the
// format of the incoming JSON from clients.
type InMessage struct {
	// The action defines what the client wants the
	// server to perform.
	Action string `json:"action"`

	// The room defines in which room the client
	// wants it performed.
	Room string `json:"channel"`

	// The message contains the string content of a
	// particular action.
	Message json.RawMessage `json:"message"`
}

// The OutMessage is the structure that controls the
// format of the outgoing JSON from the server.
type OutMessage struct {
	// The sender type defines whether the message
	// is being sent by a `user`, a `room` or a `server`
	SenderType string `json:"sender_type"`

	// The sender defines the name of the user that
	// is broadcasting the message.
	Sender string `json:"sender"`

	// The message defines the string contents of the
	// client's message.
	Message json.RawMessage `json:"message"`
}

// The ListMessage is the structure that controls
// the format of the outgoing lists JSON
// from the server.
type ListMessage struct {
	// The sender type defines whether the message
	// is being sent by a `user`, a `room` or a `server`
	SenderType string `json:"sender_type"`

	// The message defines the list contents of the
	// message.
	Message []string `json:"message"`
}

// This function parses the JSON input of a message
// received from the client and executes the appropriate
// actions required by the server.
func handlePayload(payload []byte, client *Client) {
	msg := InMessage{}
	err := json.Unmarshal(payload, &msg)

	if err != nil {
		log.Println("Message with Incorrect Payload by", client.id)
		return
	}

	switch msg.Action {
	case ACTION_ROOMS:
		sendRooms(client)

	case ACTION_JOIN:
		if _, found := client.server.rooms[msg.Room]; found {
			if client.room != nil {
				client.room.leave <- client
			}

			client.server.rooms[msg.Room].join <- client
		}

	case ACTION_LEAVE:
		if client.room != nil {
			client.room.leave <- client
			client.room = nil
		}

	case ACTION_NAME:
		bytes := msg.Message[1:int(math.Min(float64(len(msg.Message)-1), NAME_MAX_SIZE))]
		nick := string(bytes)

		log.Printf("Client %s (%s) changing nickname to: %s\n", client.id, client.nickname, nick)
		client.nickname = nick

	case ACTION_SEND:
		if client.room != nil {
			send(msg.Message, client.room.name, client)
		}

	case ACTION_USERS:
		if client.room != nil {
			sendUsers(client)
		}

	case ACTION_CREATE:
		bytes := msg.Message[1:int(math.Min(float64(len(msg.Message)-1), NAME_MAX_SIZE))]
		name := string(bytes)

		if _, exists := client.server.rooms[name]; exists {
			log.Printf("Client %s (%s) tried to create a room with existing name (%s)\n", client.id,
				client.nickname, name)
		} else {
			log.Printf("Client %s (%s) created room %s\n", client.id, client.nickname, name)

			// Create room and add to server.
			room := newRoom(name, client.server.channel)
			client.server.rooms[name] = room

			// Execute new room functions.
			go room.process()
			go room.write(client.server)
			go room.read(client.server)

			syncRoom(client.server, room)
		}

	default:
		log.Printf("Client %s (%s) tried invalid action\n", client.id, client.nickname)
	}
}

// This function broadcasts a message as requested by the
// client to a specific room.
func send(message []byte, channel string, client *Client) {
	msg := OutMessage{
		SenderType: TYPE_USER,
		Sender:     client.nickname,
		Message:    message[0:int(math.Min(float64(len(message)), MESSAGE_MAX_SIZE))],
	}

	payload, err := json.Marshal(msg)
	if err != nil {
		log.Println("Error Generating Payload by", client.id)
		return
	}

	log.Printf("Client %s (%s@%s) sending message: %s\n", client.id, client.nickname, channel, msg.Message)
	client.room.messages <- payload
}

// This function broadcastss a message with the room name
// that was created so it can be synced.
func syncRoom(s *Server, r *Room) {
	msg := OutMessage{
		SenderType: TYPE_ROOM,
		Message:    []byte(fmt.Sprintf(`"%s"`, r.name)),
	}

	payload, err := json.Marshal(msg)
	if err != nil {
		log.Printf("Error Generating Payload for Room Syncing: %s\n", err)
		return
	}

	log.Printf("Syncing Room %s\n", r.name)
	s.broadcast <- payload
}

// This function sends a message to the requesting client
// containing all the rooms in the server.
func sendRooms(client *Client) {
	rooms := make([]string, 0)
	for _, room := range client.server.rooms {
		rooms = append(rooms, room.name)
	}

	msg := ListMessage{
		SenderType: TYPE_SERVER,
		Message:    rooms,
	}

	payload, err := json.Marshal(msg)
	if err != nil {
		log.Println("Error Generating Channels Payload")
		return
	}

	client.messages <- payload
}

// This function sends a message to the requesting client
// containing all the users in the user's current room.
func sendUsers(client *Client) {
	users := make([]string, 0, len(client.room.clients))
	for us := range client.room.clients {
		users = append(users, us.nickname)
	}

	msg := ListMessage{
		SenderType: TYPE_ROOM,
		Message:    users,
	}

	payload, err := json.Marshal(msg)
	if err != nil {
		log.Println("Error Generating Users Payload")
		return
	}

	client.messages <- payload
}
