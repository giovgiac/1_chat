// Copyright (c) 2019 Team 1. All Rights Reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package chat

import (
	"encoding/json"
	"log"
)

func handleRoomState(payload []byte, r *Room) {
	msg := OutMessage{}
	err := json.Unmarshal(payload, &msg)

	if err != nil {
		log.Println("Room State Received Had Incorrect Payload")
		return
	}

	if msg.SenderType == TYPE_USER {
		if r != nil {
			r.payloads <- payload
		}
	} else {
		log.Println("Sender Type in Payload Improper for Room")
		return
	}
}

func handleServerState(payload []byte, s *Server) {
	msg := OutMessage{}
	err := json.Unmarshal(payload, &msg)

	if err != nil {
		log.Println("Server State Received Had Incorrect Payload")
		return
	}

	if msg.SenderType == TYPE_ROOM {
		// Sync rooms across servers.
		if s != nil {
			found := false
			name := string(msg.Message[1 : len(msg.Message)-1])

			for _, room := range s.rooms {
				if room.name == name {
					found = true
				}
			}

			if found == false {
				log.Printf("Adding New Room: %s\n", name)

				// Add new room to server
				s.rooms[name] = newRoom(name, s.channel)

				go s.rooms[name].process()
				go s.rooms[name].write(s)
				go s.rooms[name].read(s)
			}
		}
	} else {
		log.Println("Sender Type in Payload Improper for Server")
		return
	}
}
