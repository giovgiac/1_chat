// Copyright (c) 2019 Team 1. All Rights Reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package chat

import (
	"github.com/gorilla/websocket"
	"github.com/streadway/amqp"
	"log"
	"net/http"
)

// A WebSocket object that is used to establish
// connections to requesting clients.
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// The structure that contains information about
// the server.
type Server struct {
	// A message channel to send messages to the
	// broadcast exchange of RabbitMQ.
	broadcast chan []byte

	// Stores the AMQP channel that was created to
	// interact with RabbitMQ.
	channel *amqp.Channel

	// Stores the dictionary of connected clients
	// to this server.
	clients map[*Client]bool

	// The connection with the RabbitMQ service.
	connection *amqp.Connection

	// A message channel to send messages to the
	// exchange of RabbitMQ.
	messages chan []byte

	// The broadcast queue of RabbitMQ.
	queue amqp.Queue

	// Defines a go room for clients that are
	// seeking to register with the server.
	register chan *Client

	// Stores the main room of this particular
	// server.
	//room *Room
	rooms map[string]*Room

	// Defines a go room for clients that are
	// seeking to unregister with the server.
	unregister chan *Client
}

// A function that creates and returns a pointer to
// a new server with a main room called as named in the argument.
func NewServer(conn *amqp.Connection, ch *amqp.Channel, name string) *Server {
	server := Server{
		broadcast:  make(chan []byte),
		channel:    ch,
		clients:    make(map[*Client]bool),
		connection: conn,
		messages:   make(chan []byte, 256),
		register:   make(chan *Client),
		rooms:      make(map[string]*Room),
		unregister: make(chan *Client),
	}

	// Attaches itself to the broadcast exchange in RabbitMQ.
	err := ch.ExchangeDeclare(
		"broadcast",
		"fanout",
		true,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		return nil
	}

	server.queue, err = ch.QueueDeclare(
		"",
		false,
		false,
		true,
		false,
		nil,
	)

	if err != nil {
		return nil
	}

	err = ch.QueueBind(
		server.queue.Name,
		"",
		"broadcast",
		false,
		nil,
	)

	if err != nil {
		return nil
	}

	// Creates the main room with the given name.
	server.rooms[name] = newRoom(name, ch)

	// Create threads for server reading and writing.
	go server.write()
	go server.read()

	return &server
}

// A function that processes all the messages in the
// server's go channels, taking appropriate action
// as it comes across each of them.
//
// WARNING: THIS FUNCTION SHOULD BE CALLED ON A SEPARATE THREAD.
func (s *Server) Process() {
	// Create thread for channel
	// go s.room.process()
	for _, room := range s.rooms {
		go room.process()
	}

	for {
		select {
		case client := <-s.register:
			s.clients[client] = true

			log.Printf("Client %s joined the server\n", client.id)
			log.Printf("Total Connections: %d\n", len(s.clients))
		case client := <-s.unregister:
			if _, found := s.clients[client]; found {
				delete(s.clients, client)
				close(client.messages)

				log.Printf("Client %s disconnect from the server\n", client.id)
				log.Printf("Total Connections: %d\n", len(s.clients))
			}
		}
	}
}

func (s *Server) read() {
	defer func() {
		s.channel.Close()
		s.connection.Close()
	}()

	// Sync messages across rooms.
	for _, room := range s.rooms {
		go room.read(s)
	}

	// Sync rooms across servers.
	msgs, err := s.channel.Consume(
		s.queue.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		log.Fatal(err)
		return
	}

	for {
		for msg := range msgs {
			log.Printf("Received Message from Exchange 'broadcast': %s\n", msg.Body)
			handleServerState(msg.Body, s)
		}
	}
}

func (s *Server) write() {
	defer func() {
		s.channel.Close()
		s.connection.Close()
	}()

	// Sync messages across rooms.
	for _, room := range s.rooms {
		go room.write(s)
	}

	// Sync rooms across servers.
	for {
		select {
		case msg := <-s.broadcast:
			err := s.channel.Publish(
				"broadcast",
				"",
				false,
				false,
				amqp.Publishing{
					ContentType: "text/plain",
					Body:        msg,
				},
			)

			if err != nil {
				log.Printf("Error while Sending Message to Exchange 'broadcast': %s\n", msg)
				return
			}
		}
	}
}

// The function that serves the WebServer application
// to clients, allowing them to connect and use the
// chat service.
func ServeWebServer(server *Server, w http.ResponseWriter, r *http.Request, room string) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal(err)
		return
	}

	// Create a new client connection with server
	client := newClient(server, conn)
	server.register <- client

	// By default, join standard room
	server.rooms[room].join <- client
	syncRoom(server, server.rooms[room])

	// Create threads for reading and writing
	go client.write()
	go client.read()
}
