// Copyright (c) 2019 Team 1. All Rights Reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package main

import (
	"bufio"
	"flag"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"math/rand"
	"net/url"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"
)

const (
	//
	//
	BOT_TIME = 4
)

// Flags and global variables that are used to configure the chatbot.
var (
	//
	//
	addr = flag.String("addr", ":8080", "the ip address and port to use with the service")
)

// The Message is the structure that controls the
// format of the incoming JSON from clients.
type Message struct {
	// The action defines what the client wants the
	// server to perform.
	Action string `json:"action"`

	// The room defines in which room the client
	// wants it performed.
	Room string `json:"channel"`

	// The message contains the string content of a
	// particular action.
	Message string `json:"message"`
}

func randomString(length int) string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

	bytes := make([]rune, length)
	for i := range bytes {
		bytes[i] = letters[rand.Intn(len(letters))]
	}

	return string(bytes)
}

func randomMessage() Message {
	return Message{
		Action:  "send",
		Room:    "",
		Message: randomString(20),
	}
}

func createBot(room string) {
	// Connect to WebSocket server.
	u := url.URL{Scheme: "ws", Host: *addr, Path: "/ws"}
	conn, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatal("Failed to create the WebSocket connection:", err)
	}

	name := Message{
		Action:  "name",
		Room:    "",
		Message: randomString(5),
	}

	// Create random name for bot.
	_ = conn.WriteJSON(name)

	server := Message{
		Action:  "join",
		Room:    room,
		Message: "",
	}

	// Join assigned room.
	_ = conn.WriteJSON(server)

	for {
		// Send random message to server.
		msg := randomMessage()
		_ = conn.WriteJSON(msg)

		// Wait so as not to spam the server.
		time.Sleep(BOT_TIME * time.Second)
	}
}

func main() {
	flag.Parse()

	// Enable all core usage.
	runtime.GOMAXPROCS(runtime.NumCPU())

	// Create reader and print header message.
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("1Chat Bot")
	fmt.Println("------------------------")

	for {
		fmt.Print(">: ")
		cmd, _ := reader.ReadString('\n')
		cmd = strings.Replace(cmd, "\n", "", -1)
		args := strings.Split(cmd, " ")

		if strings.Compare(args[0], "create") == 0 {
			num, _ := strconv.Atoi(args[1])
			for i := 0; i < num; i++ {
				go createBot(args[2])
				time.Sleep(100 * time.Millisecond)
			}
		} else if strings.Compare(args[0], "exit") == 0 {
			break
		}
	}
}
