# Server

A distributed server built in Go for a safe and multithreaded server that communicates over
WebSockets and trades information through JSON.

## Execution

First, try to execute the server through the provided shell script, by running:
```sh
$ cd ..
$ sh run-server.sh
```

If that doesn't work, make sure that the project path is in the $GOPATH variable. If it 
isn't, you can execute the following commands, where ${PROJECT_DIR} is the path of the project:
```sh
$ export GOPATH="$GOPATH:${PROJECT_DIR}/server"
$ go run server
```

Otherwise, execute the following command:

```sh
$ go run server
```

Note that, you should execute the code from the 1_chat folder (where client and server
are located) for the HTML to be correctly served.
