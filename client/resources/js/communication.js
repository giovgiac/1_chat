/**
 * @file communication.js
 *
 * Manages events to communicate with the service(register, join channel, etc)
 *
 */

// Code is loaded after the DOM is using windows.onload
var ws;

function changeChannel(channel){
        if (!ws) {
            return false;
        }
        var server_msg = {
            action: "join",
            channel: channel
        };

        ws.send(JSON.stringify(server_msg));
        chat.innerHTML = "";
        return false;
}


window.onload = function(){
    // Chat element
    var chat = document.getElementById("chat");

	// List of channels
	var channels = document.getElementById("channels");

	// List of users
	var users = document.getElementById("users");

    // Get the channels periodically
    setInterval(getChannels, 5000);


    function getChannels() {
        if (!ws) {
            return false;
        }
        var server_msg = {
            action: "channels"
        };

        ws.send(JSON.stringify(server_msg));
        return false;
    }

    // Function to append messages to the chat window
    function appendChat(item) {
        var doScroll = true;
        chat.appendChild(item);
        if (doScroll) {
            document.getElementById("scrollChat").scrollTop = document.getElementById("scrollChat").scrollHeight
        }
    }

	// Function to append channels to the list of channels window
    function appendChannel(item) {
        var doScroll = true;
        channels.innerHTML = "";
        var channelList = [];
        item.sort();
        item.forEach(function(element, index){
            channelList.push(document.createElement("input"));
            channelList[index].type = "button";
            channelList[index].value = element;
            channelList[index].setAttribute("class", "btn btn-primary");
            channelList[index].setAttribute("onclick", "changeChannel(\""+ element +"\")");
        });
        channelList.forEach(function(element){
            channels.appendChild(element);
            channels.appendChild(document.createElement("br"));
        });
        if (doScroll) {
            document.getElementById("channels").scrollTop = document.getElementById("channels").scrollHeight
        }
    }

	

    // Event listener/handler for the name changing button
    document.getElementById("change-name").onsubmit = function () {
        var username = document.getElementById("username");
        if (!ws) {
            return false;
        }
        if (!username.value) {
            return false;
        }
        var server_msg = {
            action: "name",
            message: username.value
        };
        ws.send(JSON.stringify(server_msg));
        document.getElementById("nick").innerText = "Welcome " + username.value;
        username.value = "";
        return false;
    };

    document.getElementById("create-channel").onsubmit = function () {
        var channelName = document.getElementById("createChannel");
        if (!ws) {
            return false;
        }
        if (!channelName.value) {
            return false;
        }
        var server_msg = {
            action: "create",
            message: channelName.value
        };
        ws.send(JSON.stringify(server_msg));
        channelName.value = "";
        return false;
    };

    // Event listener/handler for sending messages
    document.getElementById("send-message").onsubmit = function () {
        var msg = document.getElementById("msg");
        if (!ws) {
            return false;
        }
        if (!msg.value) {
            return false;
        }
        var server_msg = {
            action: "send",
            message: msg.value,
        };
        ws.send(JSON.stringify(server_msg));
        msg.value = "";
        return false;
    };

    /**
     * Deals with the basic websocket functions(opening, closing and listening)
     */

    // Opens the websocket
    ws = new WebSocket("ws://" + document.location.host + "/ws");

    /**
     * Handles behavior when closing the connection to the WebSocket server.
     */
    ws.onclose = function (evt) {
        console.log("Disconnected from Server");
    };

    /**
     * Handles behavior when receiving messages from the WebSocket server.
     * Can receive a message, the list of channels or the users in the channel
     */

    ws.onmessage = function (evt) {
        var messages = evt.data.split('\n');
        for (var i = 0; i < messages.length; i++) {
            var item = document.createElement("div");
            parsed = JSON.parse(messages[i])  
            if(parsed.hasOwnProperty('sender_type')){
                //the user will send a chat message
                if (parsed.sender_type == 'user') {
                    var sender = parsed.sender;
                    var message = parsed.message;
                    item.innerText = sender + ": " + message;
                    appendChat(item);
                //the server will inform the available channels
                } else if (parsed.sender_type == 'server') {
                    var channel = parsed.message;
                    appendChannel(channel);
                //the channel will inform the connected users
                }
            }
        }
    };

    /**
     * Handles behavior when opening the connection to the WebSocket server.
     */
    ws.onopen = function (evt) {
        console.log("Connected to Server");
    };
};
