# Copyright (c) 2019 Team 1. All Rights Reserved.
# Use of this source code is governed by a MIT-style
# license that can be found in the LICENSE file.

FROM golang:1.12.5

COPY client/ /go/client/
COPY server/src/ /go/src/
RUN go get -d -v ./...
RUN go install -v ./...

EXPOSE 8080
CMD ["server"]
