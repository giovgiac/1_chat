#!/usr/bin/env bash

set -e

# Start RabbitMQ from entry point.
./docker-entrypoint.sh rabbitmq-server -detached

# Join cluster.
rabbitmqctl stop_app
rabbitmqctl join_cluster rabbit@rabbit-master

# Stop RabbitMQ so that we can make it run in foreground.
rabbitmqctl stop
sleep 2s

# Start again.
rabbitmq-server
